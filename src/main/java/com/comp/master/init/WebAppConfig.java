package com.comp.master.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("com.comp.master")
public class WebAppConfig extends WebMvcConfigurerAdapter {
	
	final Logger LOG = LoggerFactory.getLogger(WebAppConfig.class);
	
	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		LOG.info("initializing view resolver...");
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/pages/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		LOG.info("initialized view resolver");
		return resolver;
	}

}
